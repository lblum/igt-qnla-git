/**
 * 
 */
package com.igt.QNLA.Main.visuals;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;;

/**
 * @author 0
 *
 */
public class NumberButton extends Button {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String text;
	public NumberButton() {
		super();
		init();
	}


	public NumberButton(String text) {
		super(text);
		this.text = text;
		init();
	}
	
	
	private void init() {
		// Default aspect values
		this.setForeground(new Color(153, 204, 255));
		this.setBackground(new Color(0, 102, 153));
		if (this.text.length()>2){
			this.setFont(new Font("Verdana", Font.BOLD, 9));			
			
		} else {
			this.setFont(new Font("Verdana", Font.BOLD, 18));			
		}
	
	}

}
